<?php

    require('animal.php');
    require('frog.php');
    require('ape.php');

    $sheep = new Animal("Shaun");
    echo "Name : " . $sheep->nama ."<br>";
    echo "Legs : " . $sheep->kaki ."<br>";
    echo "Cold_Blooded : " . $sheep->cold_blooded ."<br><br>";
    
    $kodok = new Frog("Buduk");
    echo "Name: " . $kodok->nama ."<br>";
    echo "Legs : " . $kodok->kaki ."<br>";
    echo "Cold_Blooded : " . $kodok->cold_blooded . "<br>";
    $kodok->jump(); // "hop hop"

    $sungokong = new Ape("Kera sakti");
    echo "Name : " . $sungokong->nama ."<br>";
    echo "Legs : " . $sungokong->kaki ."<br>";
    echo "Cold_Blooded : " . $sungokong->cold_blooded . "<br>";
    $sungokong->yell();  // "Auooo"


?>



